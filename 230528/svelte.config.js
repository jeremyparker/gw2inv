// import adapter from '@sveltejs/adapter-auto';
import adapter from '@sveltejs/adapter-static';
import preprocess from 'svelte-preprocess';
import path from 'path';

const dev = process.argv.includes('dev');

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: preprocess(),

	kit: {
		adapter: adapter({
			fallback: 'index.html'
		}),
		paths: {
			base: dev ? '' : '/gw2inv'
		},
		// prerender: { entries: [] },
		alias: {
			$lib: path.resolve('./src/lib'),
			$src: path.resolve('./src'),
			$data: path.resolve('./src/data')
		}
	}
};

export default config;
