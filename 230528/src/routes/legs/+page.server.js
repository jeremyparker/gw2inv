import legendaries from '$data/legendaries.json';
import precursors from '$data/precursors.json';

const legoIds = legendaries.map((l) => l[0]).join();
const itemDataUrl = 'https://api.guildwars2.com/v2/items?ids=' + legoIds;
const priceDataUrl = 'https://api.guildwars2.com/v2/commerce/prices?ids=' + legoIds;
const craftPriceUrl = 'https://www.gw2spidy.com/api/v0.9/json/item/96915';

// /api/{version}/{format}/recipe/{dataId}
// /api/v0.9/json/recipe/13705

// /api/{version}/{format}/item/{dataId}
// /api/v0.9/json/item/96915

let itemData = [],
	priceData = [],
	craftPriceData = [];

export async function getData() {
	itemData = await (await fetch(itemDataUrl)).json();
	priceData = await (await fetch(priceDataUrl)).json();
	craftPriceData = await (await fetch(craftPriceUrl)).json();
	console.log(itemData);
	return [itemData, priceData, craftPriceData];
}

// const recipeUrl = ['https://api.guildwars2.com/v2/recipes/', '?v=2022-03-09T02:00:00.000Z'];
