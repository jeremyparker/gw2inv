import accData from '$src/data.json'

import itemList from '$data/itemList.json'
let list = itemList.map(i => i[0])

export async function load() {
    let accountData = []
    let retVal = []
    await Promise.all(accData.map(async (d) => {
        let retObj = d
        const materialsCall = await fetch(`https://api.guildwars2.com/v2/account/materials?access_token=${d.apikey}`);
        const bankCall = await fetch(`https://api.guildwars2.com/v2/account/bank?access_token=${d.apikey}`);
        const materialsData = await materialsCall.json();
        const bankData = await bankCall.json();
        const charData = await getCharData(d)
        const dedupedData = processData(materialsData,bankData, charData)
        retObj.items = dedupedData
        accountData.push(dedupedData)
        retVal.push(retObj)

    }));
	return retVal;
}

async function getCharData (data) {
    let retVal = []
    await Promise.all(data.characters.map(async (d) => {
        let invurl = `https://api.guildwars2.com/v2/characters/${d}/inventory?access_token=${data.apikey}`
        const invCall = await fetch(invurl);
        const invData = await invCall.json();
        let invFlat = invData.bags.map(bag => bag?.inventory.filter(i => i)).flat().filter(item => item && 'id' in item && list.includes(item.id))
        if (invFlat.length){
            retVal.push(invFlat)
        }
    }));
    return retVal.filter(i => i.length)
}

function processData (mats, bank, invs){
    invs = invs.flat()
    let itemData = list.map(item => {
        let retCount = 0
        let baseArr = []
        let matItem = mats?.filter(m => m?.id === item)
        let bankItem = bank?.filter(b => b?.id=== item)
        let invsItems = invs?.filter(i => i?.id === item)
        let itemMonster = baseArr.concat(matItem, bankItem, invsItems)
       
        for (let i in itemMonster){
            retCount = retCount + itemMonster[i].count

        }
        return [item, retCount]
    })
    return itemData
}