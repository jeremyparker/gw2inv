import accts from '$data/accts.json'
import items from '$data/items.json'
let itemids = items.map(m => m[0])
const itemObj = Object.fromEntries(items.map(k => [k[0], Object.fromEntries(accts.map(m => [m.name, 0] ))])); 
export const load = async ({ fetch }) => {
  for (let i = 0; i < accts.length ; i++){
  let allTheRest = []
  let acctData = []
    const response = await fetch(`https://api.guildwars2.com/v2/characters?v=latest&page=0&page_size=200&access_token=${accts[i].api}`)
    const resr = await response.json()
    console.log('i',i)
    console.log('resr[0].name',resr[0].name)
    acctData.push(resr)
    const bankd = await fetch(`https://api.guildwars2.com/v2/account/bank?access_token=${accts[i].api}`)
    const bankr = await bankd.json()
    const shinvd = await fetch(`https://api.guildwars2.com/v2/account/inventory?access_token=${accts[i].api}`)
    const shinvr = await shinvd.json()
    const matsd = await fetch(`https://api.guildwars2.com/v2/account/materials?access_token=${accts[i].api}`)
    const matsr = await matsd.json()
    allTheRest = [...bankr, ...shinvr, ...matsr]
  
    for (let accountNo = 0; accountNo < acctData.length ; accountNo++){
      for (let charNo = 0; charNo < acctData[accountNo].length; charNo++){
        console.log(acctData[accountNo][charNo].name)
        let bags = acctData[accountNo][charNo].bags.filter(f => f)
        // console.log('bags', bags)
        for (let bagNo = 0; bagNo < bags.length; bagNo++){
          let inventory = bags[bagNo].inventory.filter(f => f)
          for (let itemNo = 0; itemNo < inventory.length; itemNo++){
            let item = inventory[itemNo] 
            // console.log(item)
            if (item.id === 43766){
              console.log(item)
              console.log('itemObj[item.id][accts[i].name] ',itemObj[item.id][accts[i].name] )
            }
            if (item && itemids.indexOf(item.id) > -1){
              itemObj[item.id][accts[i].name] += item.count
              console.log('item.id',item.id )
              console.log('itemObj[item.id][accts[i].name] ',itemObj[item.id][accts[i].name] )
            }
          }
        }
      }
    }
    // metalswing = actual: 111, counted: 111 (100 bank, 11 chars)
    // frumps = actual: 96, counted: 107 (66 bank, 30 chars)
    // nroto = actual: 37, counted: 78 (0 bank, 37 chars)
    // jparker = actual: 20, counted: 98 (10 bank, 10 chars)
    // c2_1 = actual: 83, counted: 181 (4 bank, 79 chars)
    // c2_2= actual: 83, counted: 260 (0 bank, 83 chars)
    // c2_3 = actual: 83, counted: 343 (0 bank, 83 chars)
    // c2_4 = actual: 83, counted: 426 (0 bank, 83 chars)
    // c2_5 = actual: 83, counted: 509 (60 bank, 23 chars)
    for (let itemNo = 0; itemNo < allTheRest.length; itemNo++){
      let item = allTheRest[itemNo]
      if (item && itemids.indexOf(item.id) > -1){
        itemObj[item.id][accts[i].name] += item.count
      }
    }
  }
    // let aio = acctData.map(resr => resr.map(char => console.log(char.name) && char.bags.filter(f => f))
    //   .map(bag => {
    //     if (bag && bag.inventory){
    //       let inves = bag.inventory.map(invitem => {
    //         console.log(invitem)
    //         if (invitem && itemids.indexOf(invitem.id) > -1){
    //           if (invitem.id == 43766) console.log('tome count',invitem.count) 
    //           itemObj[invitem.id][accts[i].name] = parseInt(itemObj[invitem.id][accts[i].name]) + parseInt(invitem.count)
    //         }
    //
    //     })
    //
    //       }
    //     })
    //   )
    
  // const bankd = await fetch(`https://api.guildwars2.com/v2/account/bank?access_token=${accts[i].api}`)
  // const bankr = await bankd.json()
  // const shinvd = await fetch(`https://api.guildwars2.com/v2/account/inventory?access_token=${accts[i].api}`)
  // const shinvr = await shinvd.json()
  // const matsd = await fetch(`https://api.guildwars2.com/v2/account/materials?access_token=${accts[i].api}`)
  // const matsr = await matsd.json()
  // allTheRest = [...bankr, ...shinvr, ...matsr].filter(item => item && itemids.indexOf( item.id ) > -1).map(item => {
  //   itemObj[item.id][accts[i].name] = parseInt(itemObj[item.id][accts[i].name]) + parseInt(item.count)
  // })
  // return [] 
  return { itemObj }
}

