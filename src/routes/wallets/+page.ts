import {json} from '@sveltejs/kit'
import accts from '$data/accts.json'
/** @type {import('./$types').PageServerLoad} */
// let apikey, name
// const apiUrls = {
//   bank:`https://api.guildwars2.com/v2/account/bank?access_token=${apikey}`,
//   inventory: `https://api.guildwars2.com/v2/characters/${name}/inventory?access_token=${apikey}`,
//   materials: `https://api.guildwars2.com/v2/account/materials?access_token=${apikey}`,
//   wallet: `https://api.guildwars2.com/v2/account/wallet?access_token=${apikey}`,
//   gen: {
//     currencies: `https://api.guildwars2.com/v2/currencies?ids=all`,
//     items: ``
//   }
// }

export const load = async ({ fetch }) => {


  let retData = []
  for (let i = 0; i < accts.length ; i++){
    // console.log("i",i)
    // console.log("aaccts i",accts[i])
    const response = await fetch(`https://api.guildwars2.com/v2/account/wallet?access_token=${accts[i].api}`)
    retData[i] = await response.json()
  }
  return {retData}
}

