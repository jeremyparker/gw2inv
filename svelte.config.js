import adapter from '@sveltejs/adapter-static';
import { vitePreprocess } from '@sveltejs/kit/vite';
import path from 'path'
/** @type {import('@sveltejs/kit').Config} */
const config = {
	preprocess: vitePreprocess(),
	kit: {
		adapter: adapter(),
    alias: {
      $data: path.resolve('./src/data'),
      $lib: path.resolve('./src/lib')
    },
	paths: {
		base: '/gw2inv'
	},

	}
};

export default config;
